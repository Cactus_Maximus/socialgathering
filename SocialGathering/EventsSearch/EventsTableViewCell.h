//
//  EventsTableViewCell.h
//  SocialGathering
//
//  Created by Temp on 2017/09/06.
//  Copyright © 2017 Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *eventDescription;
@property (weak, nonatomic) IBOutlet UILabel *eventName;
@property (weak, nonatomic) IBOutlet UIImageView *eventImage;
@end
