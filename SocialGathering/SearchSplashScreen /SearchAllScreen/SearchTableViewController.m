//
//  SearchTableViewController.m
//  SocialGathering
//
//  Created by Temp on 2017/09/06.
//  Copyright © 2017 Temp. All rights reserved.
//

#import "SearchTableViewController.h"
#import "PlacesTableViewCell.h"
#import "EventsTableViewCell.h"
#import "GroupTableViewCell.h"
#import "PageTableViewCell.h"

@interface SearchTableViewController ()

@end

@implementation SearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //To do
    //Obviously this needs to change, logic needs to be rewritten to supple the correct height for the cell
    if(indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 9){
        return 400;
    }else if(indexPath.row == 1 || indexPath.row == 7 || indexPath.row == 10){
        return 300;
    }else if(indexPath.row % 2 == 0){
        return 620;
    }else{
        return 369;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //This logic is obviously terrible and will be changed
    //Im thinking I semi-randomly populate an array and use the array to pull out the objects,
    //and then pull and populate the corresponding cell
    if(indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 9){
        GroupTableViewCell *groupCell = [tableView dequeueReusableCellWithIdentifier:@"groupCell" forIndexPath:indexPath];
        return groupCell;
    }else if(indexPath.row == 1 || indexPath.row == 7 || indexPath.row == 10){
        PageTableViewCell *pageCell = [tableView dequeueReusableCellWithIdentifier:@"pageCell" forIndexPath:indexPath];
        return pageCell;
    }else if(indexPath.row % 2 == 0){
        PlacesTableViewCell *placeCell = [tableView dequeueReusableCellWithIdentifier:@"placeCell" forIndexPath:indexPath];
        //cell2.testTwo.text = @"Success";
        return placeCell;
    }else{
        EventsTableViewCell *eventCell = [tableView dequeueReusableCellWithIdentifier:@"eventCell" forIndexPath:indexPath];
        return eventCell;
    }
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
