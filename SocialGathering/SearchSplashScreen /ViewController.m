//
//  ViewController.m
//  SocialGathering
//
//  Created by Temp on 2017/08/30.
//  Copyright © 2017 Temp. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "FacebookSearchUtil.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <TwitterKit/TwitterKit.h>
#import "PagesTableViewController.h"
#import "PlacesTableViewController.h"
#import "EventsTableViewController.h"
#import "SearchTableViewController.h"

#import "GroupsTableViewController.h"
#import "Groups.h"
#import <CoreLocation/CoreLocation.h>


@interface ViewController()

@end

@implementation ViewController
CLLocationManager  *locationManager;

//This should be removed
- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
}

//This should reside in FacebookSearchUtil, like the rest of the button methods,
//I pulled this out during testing
-(void)getGroupsByTerm:(NSString *) searchTerm{
    NSMutableDictionary *pagesSearch = [NSMutableDictionary dictionaryWithCapacity:3L];
    [pagesSearch setObject:@"37.416382,-122.152659" forKey:@"center"];
    [pagesSearch setObject:@"group" forKey:@"type"];
    [pagesSearch setObject:@"1000000" forKey:@"distance"];
    
    NSString *searchString = [NSString stringWithFormat:@"/search?q=%@",searchTerm];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:searchString parameters:pagesSearch HTTPMethod:@"GET"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, NSDictionary* result, NSError *error) {
        
        NSArray *arrayResult = [result objectForKey:@"data"];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        GroupsTableViewController *secondViewController =  (GroupsTableViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"GroupTableViewController"];
        secondViewController.groupsArray = arrayResult;
        
        [[self navigationController] pushViewController:secondViewController animated:YES];
        
        if(error != nil){
            NSLog(@"error %@",error);
        }
    }];
}

- (IBAction)searchGroupsClicked:(UIButton *)sender{
    NSString *output = _searchOutlet.text;
    [self getGroupsByTerm:output];
}

- (IBAction)pageSearchClicked:(UIButton *)sender{
    NSString *output = _searchOutlet.text;
    
    [_facebookSearchUtility getPagesByTerm:output];
    // To Do
    // Only seque if data is ready
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    PagesTableViewController *secondViewController =  (PagesTableViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"PagesTableViewController"];
    [[self navigationController] pushViewController:secondViewController animated:YES];
    
}

- (IBAction)placesButtonClicked:(UIButton *)sender{
    NSString *output = _searchOutlet.text;
    
    [_facebookSearchUtility getPlacesByTerm:output];
    // To Do
    // Only seque if data is ready
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    PlacesTableViewController *secondViewController =  (PlacesTableViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"PlacesTableViewController"];
    [[self navigationController] pushViewController:secondViewController animated:YES];
    
}


- (IBAction)eventsButtonClicked:(UIButton *)sender {
    NSString *output = _searchOutlet.text;
    
    
    [_facebookSearchUtility getEventsByTerm:output];
    
    // To Do
    // Only seque if data is ready
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    EventsTableViewController *secondViewController =  (EventsTableViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"EventsTableViewController"];
    [[self navigationController] pushViewController:secondViewController animated:YES];
}

- (IBAction)getText:(UIButton *)sender {
    NSString *output = _searchOutlet.text;
    
    //Should probably change to a batch
    [_facebookSearchUtility getPlacesByTerm:output];
    [_facebookSearchUtility getGroupsByTerm:output];
    [_facebookSearchUtility getPagesByTerm:output];
    [_facebookSearchUtility getEventsByTerm:output];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    SearchTableViewController *searchTableViewController =  (SearchTableViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"SearchTableViewController"];
    
    [[self navigationController] pushViewController:searchTableViewController animated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.readPermissions = @[@"email"];
    loginButton.center = CGPointMake(190, 650);
    [self.view addSubview:loginButton];
    
     _facebookSearchUtility = [[FacebookSearchUtil alloc]init];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
