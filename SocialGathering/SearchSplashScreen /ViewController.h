//
//  ViewController.h
//  SocialGathering
//
//  Created by Temp on 2017/08/30.
//  Copyright © 2017 Temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookSearchUtil.h"

@interface ViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *searchOutlet;
@property (strong, nonatomic) FacebookSearchUtil *facebookSearchUtility;
@end

