//
//  PlacesTableViewCell.h
//  SocialGathering
//
//  Created by Temp on 2017/09/05.
//  Copyright © 2017 Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlacesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *placeTitle;
@property (weak, nonatomic) IBOutlet UIImageView *placeImage;
@property (weak, nonatomic) IBOutlet UITextView *placeDescription;
@end
