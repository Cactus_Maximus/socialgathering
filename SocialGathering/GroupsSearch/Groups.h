//
//  Groups.h
//  SocialGathering
//
//  Created by Temp on 2017/09/05.
//  Copyright © 2017 Temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Groups : NSObject
+(instancetype) sharedInstance;
@property (nonatomic, weak) NSArray *groupData;
@property(nonatomic, weak) NSString *groupTitle;
-(instancetype)init NS_UNAVAILABLE;

@end
