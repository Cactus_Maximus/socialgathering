//
//  GroupsTableViewController.m
//  SocialGathering
//
//  Created by Temp on 2017/09/04.
//  Copyright © 2017 Temp. All rights reserved.
//

#import "GroupsTableViewController.h"
#import "GroupTableViewCell.h"
#import "Groups.h"

@interface GroupsTableViewController ()

@end

@implementation GroupsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_groupsArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 3;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupCell" forIndexPath:indexPath];
   
    //Test code
    //This would obviously be replaced to loop through the array instead
    cell.titleLabel.text = [[_groupsArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    NSString *privacyStatus = [[_groupsArray objectAtIndex:indexPath.row] objectForKey:@"privacy"];
    NSString *privacyLabel = [NSString stringWithFormat:@"%@%@%@",@"(", privacyStatus, @")"];
    cell.statusLabel.text = privacyLabel;
    
    return cell;
}
@end
