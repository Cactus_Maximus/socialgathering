//
//  Groups.m
//  SocialGathering
//
//  Created by Temp on 2017/09/05.
//  Copyright © 2017 Temp. All rights reserved.
//

#import "Groups.h"

@implementation Groups
+(instancetype)sharedInstance{
    static dispatch_once_t onceToken;
    static Groups *instance;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
    
}

-(instancetype)init{
    self = [super init];
    return self;
    
}

@end
