//
//  GroupTableViewCell.h
//  SocialGathering
//
//  Created by Temp on 2017/09/04.
//  Copyright © 2017 Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@end
