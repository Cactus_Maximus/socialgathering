//
//  GroupsTableViewController.h
//  SocialGathering
//
//  Created by Temp on 2017/09/04.
//  Copyright © 2017 Temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Groups.h"

@interface GroupsTableViewController : UITableViewController
@property (nonatomic, strong) NSArray *groupsArray;
@end
