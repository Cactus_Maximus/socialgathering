//
//  FacebookSearchUtil.m
//  SocialGathering
//
//  Created by Temp on 2017/09/01.
//  Copyright © 2017 Temp. All rights reserved.
//

#import "FacebookSearchUtil.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "Groups.h"
@implementation FacebookSearchUtil

-(void)getGroupsByTerm:(NSString *) searchTerm{
    NSMutableDictionary *pagesSearch = [NSMutableDictionary dictionaryWithCapacity:3L];
    [pagesSearch setObject:@"37.416382,-122.152659" forKey:@"center"];
    [pagesSearch setObject:@"group" forKey:@"type"];
    [pagesSearch setObject:@"1000000" forKey:@"distance"];
    
    NSString *searchString = [NSString stringWithFormat:@"/search?q=%@",searchTerm];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:searchString parameters:pagesSearch HTTPMethod:@"GET"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, NSDictionary* result, NSError *error) {
        NSLog(@"groups RESPONSE!!! /search");
        NSLog(@"result %@",result);
        
        NSArray *arrayResult = [result objectForKey:@"data"];
        _groupsData = arrayResult;
        
        for (NSDictionary *placeForResults in arrayResult){
            NSString *output = [placeForResults objectForKey:@"name"];
            NSLog(@"%@", output);
        }
        if(error != nil){
            NSLog(@"error %@",error);
        }
    }];
}

-(void)getPagesByTerm:(NSString *) searchTerm
{
    NSMutableDictionary *pagesSearch = [NSMutableDictionary dictionaryWithCapacity:3L];
    [pagesSearch setObject:@"37.416382,-122.152659" forKey:@"center"];
    [pagesSearch setObject:@"page" forKey:@"type"];
    [pagesSearch setObject:@"1000000" forKey:@"distance"];
    
    NSString *searchString = [NSString stringWithFormat:@"/search?q=%@", searchTerm];
    
    //To Do
    //Will be implemented like Groups, segue when complete
    [[[FBSDKGraphRequest alloc] initWithGraphPath:searchString parameters:pagesSearch HTTPMethod:@"GET"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        NSLog(@"pages RESPONSE!!! /search");
        NSLog(@"result %@",result);
        if(error != nil){
            NSLog(@"error %@",error);
        }
    }];
}

-(void)getEventsByTerm:(NSString *) searchTerm
{
    NSMutableDictionary *placesSearch = [NSMutableDictionary dictionaryWithCapacity:3L];
    [placesSearch setObject:@"37.416382,-122.152659" forKey:@"center"];
    [placesSearch setObject:@"event" forKey:@"type"];
    [placesSearch setObject:@"1000000" forKey:@"distance"];
    
    NSString *searchString = [NSString stringWithFormat:@"/search?q=%@", searchTerm];
    
    //To Do
    //Will be implemented like Groups, segue when complete
    [[[FBSDKGraphRequest alloc] initWithGraphPath:searchString parameters:placesSearch HTTPMethod:@"GET"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        NSLog(@"events RESPONSE!!! /search");
        NSLog(@"result %@",result);
        if(error != nil){
            NSLog(@"error %@",error);
        }
    }];
}

-(void)getPlacesByTerm:(NSString *) searchTerm
{
    NSMutableDictionary *placesSearch = [NSMutableDictionary dictionaryWithCapacity:3L];
    [placesSearch setObject:@"37.416382,-122.152659" forKey:@"center"];
    [placesSearch setObject:@"place" forKey:@"type"];
    [placesSearch setObject:@"1000000" forKey:@"distance"];
    
    NSString *string = [NSString stringWithFormat:@"/search?q=%@",searchTerm];
    
    //To Do
    //Will be implemented like Groups, segue when complete
    [[[FBSDKGraphRequest alloc] initWithGraphPath:string parameters:placesSearch HTTPMethod:@"GET"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        NSLog(@"places RESPONSE!!! /search");
        NSLog(@"result %@",result);
        if(error != nil){
            NSLog(@"error %@",error);
        }
    }];
}

-(void)getUserProfilePic
{
    NSMutableDictionary *userParam = [NSMutableDictionary dictionaryWithCapacity:3L];
    [userParam setObject:@"user" forKey:@"type"];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me?fields=picture.type(large)" parameters:userParam HTTPMethod:@"GET"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        NSLog(@"user profile RESPONSE !!! /search");
        NSLog(@"result %@",result);
        if(error != nil){
            NSLog(@"error %@",error);
        }
    }];
}

-(void)getPictureFromId:(NSString *) facebookID
{
    NSMutableDictionary *photoParam = [NSMutableDictionary dictionaryWithCapacity:3L];
    FBSDKGraphRequest *request2 = [[FBSDKGraphRequest alloc]
                                   initWithGraphPath:@"/1871432246405672?fields=picture&metadata=1"
                                   parameters:photoParam
                                   HTTPMethod:@"GET"];
    
    [request2 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                           id result,
                                           NSError *error) {
        NSLog(@"photo retrieval from place id, works");
        NSLog(@"result %@",result);
        if(error != nil){
            NSLog(@"error %@",error);
        }
    }];
}

@end
