//
//  FacebookSearchUtil.h
//  SocialGathering
//
//  Created by Temp on 2017/09/01.
//  Copyright © 2017 Temp. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef FacebookSearchUtil_h
#define FacebookSearchUtil_h
@interface FacebookSearchUtil : NSObject
@property(nonatomic, weak) NSArray *groupsData;

-(void)getPlacesByTerm:(NSString *) searchTerm;
-(void)getEventsByTerm:(NSString *) searchTerm;
-(void)getPagesByTerm:(NSString *) searchTerm;
-(void)getGroupsByTerm:(NSString *) searchTerm;
-(void)getUserProfilePic;
-(void)getPictureFromId:(NSString *) facebookID;
@end
#endif /* FacebookSearchUtil_h */
