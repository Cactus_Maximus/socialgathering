//
//  PagesTableViewController.m
//  SocialGathering
//
//  Created by Temp on 2017/09/05.
//  Copyright © 2017 Temp. All rights reserved.
//

#import "PagesTableViewController.h"
#import "PageTableViewCell.h"

@interface PagesTableViewController ()

@end

@implementation PagesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 10;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 280;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 3;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pageCell" forIndexPath:indexPath];
    cell.pageTitle.text = @"I need to add title here";
    return cell;
}
@end
