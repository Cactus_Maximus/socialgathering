//
//  PageTableViewCell.h
//  SocialGathering
//
//  Created by Temp on 2017/09/05.
//  Copyright © 2017 Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UITextView *pageDescription;
@property (weak, nonatomic) IBOutlet UIImageView *pageImage;
@end
