//
//  main.m
//  SocialGathering
//
//  Created by Temp on 2017/08/30.
//  Copyright © 2017 Temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
